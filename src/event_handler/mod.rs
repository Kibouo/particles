// File: mod.rs
// Project: event_handler
// Created Date: 23rd December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

extern crate winit;

use camera::Camera;
use event_handler::event_flags::EventFlags;
use vulkan_objects::VulkanCoreObjects;

mod event_flags;

pub struct EventHandler
{
    pub event_flags: EventFlags,
    mouse_position:  (f64, f64)
}

impl EventHandler
{
    pub fn new() -> EventHandler
    {
        EventHandler {
            event_flags:    EventFlags::new(),
            mouse_position: (0.0, 0.0)
        }
    }

    // TODO: when pressing a button, show cursor and let it escape the window.
    // Don't do any camera movement in this mode.
    pub fn register_events(
        &mut self,
        vulkan_objects: &mut VulkanCoreObjects
    )
    {
        use winit::WindowEvent::*;

        self.event_flags.exit = false;
        self.event_flags.recreate_swapchain = false;
        self.event_flags.mouse_moved = false;

        vulkan_objects.events_loop.poll_events(|event| match event
        {
            winit::Event::WindowEvent { event, .. } => match event
            {
                Resized(..) => self.event_flags.recreate_swapchain = true,
                MouseMoved {
                    position: (x, y), ..
                } =>
                {
                    self.mouse_position = (x, y);
                    self.event_flags.mouse_moved = true;
                },
                Closed
                | KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            virtual_keycode: Some(winit::VirtualKeyCode::Escape),
                            ..
                        },
                    ..
                } => self.event_flags.exit = true,
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Pressed,
                            virtual_keycode: Some(winit::VirtualKeyCode::W),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .entry(winit::VirtualKeyCode::W)
                        .or_insert(true);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Released,
                            virtual_keycode: Some(winit::VirtualKeyCode::W),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .remove(&winit::VirtualKeyCode::W);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Pressed,
                            virtual_keycode: Some(winit::VirtualKeyCode::A),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .entry(winit::VirtualKeyCode::A)
                        .or_insert(true);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Released,
                            virtual_keycode: Some(winit::VirtualKeyCode::A),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .remove(&winit::VirtualKeyCode::A);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Pressed,
                            virtual_keycode: Some(winit::VirtualKeyCode::S),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .entry(winit::VirtualKeyCode::S)
                        .or_insert(true);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Released,
                            virtual_keycode: Some(winit::VirtualKeyCode::S),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .remove(&winit::VirtualKeyCode::S);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Pressed,
                            virtual_keycode: Some(winit::VirtualKeyCode::D),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .entry(winit::VirtualKeyCode::D)
                        .or_insert(true);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Released,
                            virtual_keycode: Some(winit::VirtualKeyCode::D),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .remove(&winit::VirtualKeyCode::D);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Pressed,
                            virtual_keycode: Some(winit::VirtualKeyCode::LAlt),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .entry(winit::VirtualKeyCode::LAlt)
                        .or_insert(true);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Released,
                            virtual_keycode: Some(winit::VirtualKeyCode::LAlt),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .remove(&winit::VirtualKeyCode::LAlt);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Pressed,
                            virtual_keycode: Some(winit::VirtualKeyCode::Space),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .entry(winit::VirtualKeyCode::Space)
                        .or_insert(true);
                },
                KeyboardInput {
                    input:
                        winit::KeyboardInput {
                            state: winit::ElementState::Released,
                            virtual_keycode: Some(winit::VirtualKeyCode::Space),
                            ..
                        },
                    ..
                } =>
                {
                    self.event_flags
                        .buttons_states
                        .remove(&winit::VirtualKeyCode::Space);
                },
                _ => ()
            },
            _ => ()
        });
    }

    // Returns a bool saying whether the application should be terminated
    pub fn evaluate_events(
        &mut self,
        vulkan_objects: &mut VulkanCoreObjects,
        camera: &mut Camera
    ) -> bool
    {
        if self.event_flags.mouse_moved
        {
            let width = vulkan_objects.dimensions[0] as f32;
            let height = vulkan_objects.dimensions[1] as f32;

            camera.tilt_over_y_axis((width / 2.0) - self.mouse_position.0 as f32);
            camera.tilt_over_x_axis((height / 2.0) - self.mouse_position.1 as f32);

            let _ = vulkan_objects
                .surface
                .window()
                .set_cursor_position((width / 2.0) as i32, (height / 2.0) as i32);
        }

        self.event_flags
            .buttons_states
            .iter()
            .for_each(|(key, _)| match *key
            {
                winit::VirtualKeyCode::W => camera.forward(),
                winit::VirtualKeyCode::A => camera.left(),
                winit::VirtualKeyCode::S => camera.backward(),
                winit::VirtualKeyCode::D => camera.right(),
                winit::VirtualKeyCode::LAlt => camera.down(),
                winit::VirtualKeyCode::Space => camera.up(),
                _ => ()
            });

        self.event_flags.exit
    }
}
