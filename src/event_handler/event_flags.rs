// File: event_flags.rs
// Project: src
// Created Date: 19th December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

extern crate winit;

use std::collections::HashMap;

#[derive(Default)]
pub struct EventFlags
{
    pub buttons_states:     HashMap<winit::VirtualKeyCode, bool>,
    pub recreate_swapchain: bool,
    pub mouse_moved:        bool,
    pub exit:               bool
}

impl EventFlags
{
    pub fn new() -> EventFlags
    {
        EventFlags {
            ..Default::default()
        }
    }
}
