// File: texture_loader.rs
// Project: src
// Created Date: 14th February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

extern crate image;

use image::RgbaImage;
use std::string::String;
use std::sync::Arc;
use vulkano::command_buffer::{AutoCommandBuffer, CommandBufferExecFuture};
use vulkano::device::Queue;
use vulkano::format::R8G8B8A8Srgb;
use vulkano::image::Dimensions;
use vulkano::image::immutable::ImmutableImage;
use vulkano::sync::NowFuture;

pub struct TextureLoader
{
    image: RgbaImage
}

impl TextureLoader
{
    pub fn open(file_path: String) -> TextureLoader
    {
        TextureLoader {
            image: image::open(file_path)
                .expect("Unable to open texture")
                .rotate180()
                .to_rgba()
        }
    }

    pub fn image_2d(
        self,
        queue: Arc<Queue>
    ) -> (
        Arc<ImmutableImage<R8G8B8A8Srgb>>,
        CommandBufferExecFuture<NowFuture, AutoCommandBuffer>
    )
    {
        let image_data = self.image.clone().into_raw();
        ImmutableImage::from_iter(
            image_data.iter().cloned(),
            Dimensions::Dim2d {
                width:  self.image.width(),
                height: self.image.height()
            },
            R8G8B8A8Srgb,
            queue
        ).expect("Unable to create ImmutableImage")
    }
}
