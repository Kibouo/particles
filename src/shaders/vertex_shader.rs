// File: vertex_shader.rs
// Project: src
// Created Date: 5th December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

#![allow(warnings)]
#[derive(VulkanoShader)]
#[ty = "vertex"]
#[src = "
#version 450

layout(push_constant) uniform pushConstants {
    mat4 vp;
} push_constants;

layout(location = 0) in mat4 translation_matrix;
layout(location = 4) in mat4 rotation_matrix;
layout(location = 8) in mat4 scaling_matrix;
layout(location = 12) in vec4 position;
layout(location = 13) in vec4 color;
layout(location = 14) in vec2 tex_coords;

layout(location = 0) out vec4 out_color;
layout(location = 1) out vec2 out_tex_coords;

void main() {
    mat4 model_matrix = translation_matrix * rotation_matrix * scaling_matrix;
    gl_Position = push_constants.vp * model_matrix * position;

    out_color = color;
    out_tex_coords = tex_coords + vec2(0.5);
}
"]
struct Dummy;
