// File: mod.rs
// Project: shaders
// Created Date: 14th February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

mod fragment_shader;
mod vertex_shader;

use std::sync::Arc;
use vulkano::device::Device;

pub struct Shaders
{
    pub vertex:   vertex_shader::Shader,
    pub fragment: fragment_shader::Shader
}

impl Shaders
{
    pub fn load(device: Arc<Device>) -> Shaders
    {
        let vertex_shader = vertex_shader::Shader::load(device.clone())
            .expect("Failed to create vertex shader module");
        let fragment_shader =
            fragment_shader::Shader::load(device).expect("Failed to create fragment shader module");

        Shaders {
            vertex:   vertex_shader,
            fragment: fragment_shader
        }
    }
}
