// File: fragment_shader.rs
// Project: src
// Created Date: 5th December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

#![allow(warnings)]
#[derive(VulkanoShader)]
#[ty = "fragment"]
#[src = "
#version 450

layout(set = 0, binding = 0) uniform sampler2D sampler2d;

layout(location = 0) in vec4 inp_color;
layout(location = 1) in vec2 tex_coords;

layout(location = 0) out vec4 out_color;

void main() {
    out_color = inp_color + texture(sampler2d, tex_coords);
}
"]
struct Dummy;
