// File: vulkan_objects.rs
// Project: src
// Created Date: 23rd February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

// File: vulkan_objects.rs
// Project: src
// Created Date: 5th December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

extern crate vulkano;
extern crate vulkano_win;
extern crate winit;

use std::sync::Arc;
use vulkano::device::{Device, DeviceExtensions, Queue};
use vulkano::image::SwapchainImage;
use vulkano::instance::{Instance, PhysicalDevice};
use vulkano::swapchain::{PresentMode, Surface, SurfaceTransform, Swapchain};
use vulkano_win::VkSurfaceBuild;
use winit::{CursorState, EventsLoop, MouseCursor, Window, WindowBuilder};

pub struct VulkanCoreObjects
{
    pub images:      Vec<Arc<SwapchainImage<Window>>>,
    pub swapchain:   Arc<Swapchain<Window>>,
    pub queue:       Arc<Queue>,
    pub device:      Arc<Device>,
    pub dimensions:  [u32; 2],
    pub surface:     Arc<Surface<Window>>,
    pub events_loop: EventsLoop
}

impl VulkanCoreObjects
{
    pub fn init() -> VulkanCoreObjects
    {
        // create vulkan instance
        let vulkan_instance = Instance::new(None, &vulkano_win::required_extensions(), None)
            .expect("Failed to create vulkan instance");

        // select device
        let physical_device = PhysicalDevice::enumerate(&vulkan_instance)
            .next()
            .expect("No device available");
        println!("Device name: {:?}", physical_device.name());

        // create window
        let events_loop = EventsLoop::new();
        let surface = WindowBuilder::new()
            .build_vk_surface(&events_loop, vulkan_instance.clone())
            .expect("Unable to build surface");
        surface.window().set_title("Particles");
        surface
            .window()
            .set_cursor_state(CursorState::Grab)
            .expect("Unable to confine cursor to this window");
        surface.window().set_cursor(MouseCursor::NoneCursor);

        // get viewport size
        let dimensions = {
            let (width, height) = surface
                .window()
                .get_inner_size_pixels()
                .expect("Unable to get window size");
            [width, height]
        };
        surface
            .window()
            .set_cursor_position((dimensions[0] / 2) as i32, (dimensions[1] / 2) as i32)
            .expect("Unable to center cursor");

        // select queue
        let queue = physical_device
            .queue_families()
            .find(|&queue| {
                queue.supports_graphics() && surface.is_supported(queue).unwrap_or(false)
            })
            .expect("No queue that can draw on our window");

        let (device, mut queues) = {
            let device_extensions = DeviceExtensions {
                khr_swapchain: true,
                ..DeviceExtensions::none()
            };

            Device::new(
                physical_device,
                physical_device.supported_features(),
                &device_extensions,
                [(queue, 0.5)].iter().cloned()
            ).expect("Failed to create device")
        };

        let queue = queues.next().unwrap();

        // create swapchain
        let (swapchain, images) = {
            let capabilities = surface
                .capabilities(physical_device)
                .expect("Failed to check surface capabilities");

            let alpha = capabilities
                .supported_composite_alpha
                .iter()
                .next()
                .expect("Unable to get supported alpha of surface");

            let format = capabilities.supported_formats[0].0;

            Swapchain::new(
                device.clone(),
                surface.clone(),
                capabilities.min_image_count,
                format,
                dimensions,
                1,
                capabilities.supported_usage_flags,
                &queue,
                SurfaceTransform::Identity,
                alpha,
                PresentMode::Fifo,
                true,
                None
            ).expect("Failed to create swapchain")
        };

        VulkanCoreObjects {
            images,
            swapchain,
            queue,
            device,
            dimensions,
            surface,
            events_loop
        }
    }
}
