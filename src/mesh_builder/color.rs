// File: color.rs
// Project: mesh_builder
// Created Date: 13th February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

#[allow(dead_code)]
#[derive(Clone, Copy)]
pub enum Color
{
    Red,
    Green,
    Blue,
    Yellow,
    Aqua,
    Fuchsia,
    White,
    Clear
}

impl Color
{
    pub fn convert(color: Color) -> [f32; 4]
    {
        match color
        {
            Color::Red => [1.0, 0.0, 0.0, 1.0],
            Color::Green => [0.0, 1.0, 0.0, 1.0],
            Color::Blue => [0.0, 0.0, 1.0, 1.0],
            Color::Yellow => [1.0, 1.0, 0.0, 1.0],
            Color::Aqua => [0.0, 1.0, 1.0, 1.0],
            Color::Fuchsia => [1.0, 0.0, 1.0, 1.0],
            Color::White => [1.0, 1.0, 1.0, 1.0],
            Color::Clear => [0.0, 0.0, 0.0, 0.0]
        }
    }
}
