// File: plane.rs
// Project: mesh_builder
// Created Date: 14th February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

use mesh_builder::color::Color;
use mesh_builder::vertex::{New as VertexNew, Vertex};
use nalgebra::{Point3, Rotation3, Translation3, Vector3};

pub struct Plane
{
    pub vertices: Vec<Vertex>,
    pub indices:  Vec<u32>
}

pub trait New<ColorType>
{
    fn new(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        width: u32,
        length: u32,
        scale: f32,
        color: ColorType,
        unused_index: u32
    ) -> Plane;
}

impl New<Color> for Plane
{
    fn new(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        width: u32,
        length: u32,
        scale: f32,
        color: Color,
        unused_index: u32
    ) -> Plane
    {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();

        let translation = Translation3::from_vector(Vector3::new(
            -(width as f32) / 2.0,
            0.0,
            -(length as f32) / 2.0
        )) * Translation3::from_vector(center.coords);

        for l in 0..=length
        {
            for w in 0..=width
            {
                vertices.push(Vertex::new(
                    translation,
                    rotation,
                    scale,
                    Point3::new(w as f32, 0.0, l as f32),
                    color
                ));
            }
        }

        for l in 0..length
        {
            for w in 0..width
            {
                let offset = unused_index + (l * (length + 1)) + w;
                indices.extend([offset + 2 + width, offset + 1, offset].iter());
                indices.extend([offset, offset + 1 + width, offset + 2 + width].iter());
            }
        }

        Plane { vertices, indices }
    }
}
