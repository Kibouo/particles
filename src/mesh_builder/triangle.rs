// File: triangle.rs
// Project: mesh_builder
// Created Date: 23rd December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

use mesh_builder::color::Color;
use mesh_builder::vertex::{New as VertexNew, Vertex};
use nalgebra::{Point3, Rotation3, Translation3};

pub struct Triangle
{
    pub vertices: Vec<Vertex>,
    pub indices:  Vec<u32>
}

pub trait New<ColorType>
{
    fn new(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        color: ColorType,
        unused_index: u32
    ) -> Triangle;
}

impl New<Color> for Triangle
{
    // The triangle is built in following order
    //        3
    //      / |
    //    /   |
    //  1 --- 2
    fn new(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        color: Color,
        mut unused_index: u32
    ) -> Triangle
    {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();

        let translation = Translation3::from_vector(center.coords);

        for coord in &[(-0.5, -0.5), (0.5, -0.5), (0.5, 0.5)]
        {
            let position = Point3::new(coord.0, coord.1, 0.0);

            // scale div 2 since using the coords as seen in the for loop, every side
            // of the figure is 2 long.
            vertices.push(Vertex::new(translation, rotation, scale, position, color));

            indices.push(unused_index);
            unused_index += 1;
        }

        Triangle { vertices, indices }
    }
}
