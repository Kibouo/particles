// File: cube.rs
// Project: mesh_builder
// Created Date: 10th February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

use mesh_builder::color::Color;
use mesh_builder::vertex::{NewWithTexCoords, Vertex};
use nalgebra::{Point2, Point3, Rotation3, Translation3};

pub struct Cube
{
    pub vertices: Vec<Vertex>,
    pub indices:  Vec<u32>
}

pub trait New<ColorType>
{
    fn new(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        color: ColorType,
        unused_index: u32
    ) -> Cube;
}

impl New<Color> for Cube
{
    fn new(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        color: Color,
        unused_index: u32
    ) -> Cube
    {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();

        let vertex_coords = [
            // front
            (0.5, 0.5, -0.5),
            (0.5, -0.5, -0.5),
            (-0.5, -0.5, -0.5),
            (-0.5, 0.5, -0.5),
            // back
            (0.5, 0.5, 0.5),
            (-0.5, 0.5, 0.5),
            (-0.5, -0.5, 0.5),
            (0.5, -0.5, 0.5),
            // left
            (-0.5, 0.5, -0.5),
            (-0.5, -0.5, -0.5),
            (-0.5, -0.5, 0.5),
            (-0.5, 0.5, 0.5),
            // right
            (0.5, 0.5, 0.5),
            (0.5, -0.5, 0.5),
            (0.5, -0.5, -0.5),
            (0.5, 0.5, -0.5),
            // top
            (0.5, 0.5, 0.5),
            (0.5, 0.5, -0.5),
            (-0.5, 0.5, -0.5),
            (-0.5, 0.5, 0.5),
            // bottom
            (-0.5, -0.5, -0.5),
            (0.5, -0.5, -0.5),
            (0.5, -0.5, 0.5),
            (-0.5, -0.5, 0.5),
        ];
        let index_offsets = [
            0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4, 8, 9, 10, 10, 11, 8, 12, 13, 14, 14, 15, 12, 16,
            17, 18, 18, 19, 16, 20, 21, 22, 22, 23, 20,
        ];

        let translation = Translation3::from_vector(center.coords);
        for (coords_done, coords) in vertex_coords.iter().enumerate()
        {
            let position = Point3::new(coords.0, coords.1, coords.2);

            if coords_done < 8
            {
                vertices.push(Vertex::new_with_tex_coords(
                    translation,
                    rotation,
                    scale,
                    position,
                    color,
                    Point2::new(coords.0, coords.1)
                ));
            }
            else if coords_done < 16
            {
                vertices.push(Vertex::new_with_tex_coords(
                    translation,
                    rotation,
                    scale,
                    position,
                    color,
                    Point2::new(coords.2, coords.1)
                ));
            }
            else
            {
                vertices.push(Vertex::new_with_tex_coords(
                    translation,
                    rotation,
                    scale,
                    position,
                    color,
                    Point2::new(coords.0, coords.2)
                ));
            }
        }

        for index_offset in index_offsets.iter()
        {
            indices.push(unused_index + index_offset);
        }

        Cube { vertices, indices }
    }
}

pub trait NewNoIndices<ColorType>
{
    fn new_no_indices(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        color: ColorType
    ) -> Cube;
}

impl NewNoIndices<Color> for Cube
{
    fn new_no_indices(
        center: Point3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        color: Color
    ) -> Cube
    {
        let mut vertices = Vec::new();
        let indices = Vec::new();

        let vertex_coords = [
            // front
            (0.5, 0.5, -0.5),
            (0.5, -0.5, -0.5),
            (-0.5, -0.5, -0.5),
            (-0.5, 0.5, -0.5),
            // back
            (0.5, 0.5, 0.5),
            (-0.5, 0.5, 0.5),
            (-0.5, -0.5, 0.5),
            (0.5, -0.5, 0.5),
            // left
            (-0.5, 0.5, -0.5),
            (-0.5, -0.5, -0.5),
            (-0.5, -0.5, 0.5),
            (-0.5, 0.5, 0.5),
            // right
            (0.5, 0.5, 0.5),
            (0.5, -0.5, 0.5),
            (0.5, -0.5, -0.5),
            (0.5, 0.5, -0.5),
            // top
            (0.5, 0.5, 0.5),
            (0.5, 0.5, -0.5),
            (-0.5, 0.5, -0.5),
            (-0.5, 0.5, 0.5),
            // bottom
            (-0.5, -0.5, -0.5),
            (0.5, -0.5, -0.5),
            (0.5, -0.5, 0.5),
            (-0.5, -0.5, 0.5),
        ];

        let translation = Translation3::from_vector(center.coords);
        for (coords_done, coords) in vertex_coords.iter().enumerate()
        {
            let position = Point3::new(coords.0, coords.1, coords.2);

            if coords_done < 8
            {
                vertices.push(Vertex::new_with_tex_coords(
                    translation,
                    rotation,
                    scale,
                    position,
                    color,
                    Point2::new(coords.0, coords.1)
                ));
            }
            else if coords_done < 16
            {
                vertices.push(Vertex::new_with_tex_coords(
                    translation,
                    rotation,
                    scale,
                    position,
                    color,
                    Point2::new(coords.2, coords.1)
                ));
            }
            else
            {
                vertices.push(Vertex::new_with_tex_coords(
                    translation,
                    rotation,
                    scale,
                    position,
                    color,
                    Point2::new(coords.0, coords.2)
                ));
            }
        }

        Cube { vertices, indices }
    }
}

impl Cube
{
    pub fn vertices_sides(&self) -> Vec<Vertex>
    {
        self.vertices
            .get(0..16)
            .expect("Unable to get side vertices from cube")
            .to_vec()
    }
    pub fn vertices_top(&self) -> Vec<Vertex>
    {
        self.vertices
            .get(16..20)
            .expect("Unable to get top vertices from cube")
            .to_vec()
    }
    pub fn vertices_bottom(&self) -> Vec<Vertex>
    {
        self.vertices
            .get(20..24)
            .expect("Unable to get bottom vertices from cube")
            .to_vec()
    }
}
