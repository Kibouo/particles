// File: vertex.rs
// Project: src
// Created Date: 6th December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

use mesh_builder::color::Color;
use nalgebra::{Matrix4, Point2, Point3, Rotation3, Translation3};

#[derive(Clone, Debug)]
pub struct Vertex
{
    pub translation_matrix: [[f32; 4]; 4],
    pub rotation_matrix:    [[f32; 4]; 4],
    pub scaling_matrix:     [[f32; 4]; 4],
    pub position:           [f32; 4],
    pub color:              [f32; 4],
    pub tex_coords:         [f32; 2]
}

pub trait New<ColorType>
{
    fn new(
        translation: Translation3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        position: Point3<f32>,
        color: ColorType
    ) -> Vertex;
}

pub trait NewWithTexCoords<ColorType>
{
    fn new_with_tex_coords(
        translation: Translation3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        position: Point3<f32>,
        color: ColorType,
        tex_coords: Point2<f32>
    ) -> Vertex;
}

impl New<Color> for Vertex
{
    fn new(
        translation: Translation3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        position: Point3<f32>,
        color: Color
    ) -> Vertex
    {
        Vertex {
            translation_matrix: translation.to_homogeneous().into(),
            rotation_matrix:    rotation.to_homogeneous().into(),
            scaling_matrix:     Matrix4::new_scaling(scale).into(),
            position:           position.to_homogeneous().into(),
            color:              Color::convert(color),
            tex_coords:         [position.x, position.y]
        }
    }
}

impl NewWithTexCoords<Color> for Vertex
{
    fn new_with_tex_coords(
        translation: Translation3<f32>,
        rotation: Rotation3<f32>,
        scale: f32,
        position: Point3<f32>,
        color: Color,
        tex_coords: Point2<f32>
    ) -> Vertex
    {
        Vertex {
            translation_matrix: translation.to_homogeneous().into(),
            rotation_matrix:    rotation.to_homogeneous().into(),
            scaling_matrix:     Matrix4::new_scaling(scale).into(),
            position:           position.to_homogeneous().into(),
            color:              Color::convert(color),
            tex_coords:         [tex_coords.x, tex_coords.y]
        }
    }
}

impl_vertex!(
    Vertex,
    translation_matrix,
    rotation_matrix,
    scaling_matrix,
    position,
    color,
    tex_coords
);
