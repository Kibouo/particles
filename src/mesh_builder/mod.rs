// File: mod.rs
// Project: mesh_builder
// Created Date: 23rd December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

use mesh_builder::vertex::Vertex;
use noise::OpenSimplex;

pub mod vertex;
mod color;
mod cube;
mod triangle;
mod plane;

pub struct MeshBuilder
{
    pub vertex_vector: Vec<Vec<Vertex>>,
    pub index_vector:  Vec<Vec<u32>>,
    noise_map:         OpenSimplex
}

impl MeshBuilder
{
    pub fn new() -> MeshBuilder
    {
        MeshBuilder {
            vertex_vector: vec![
                Vec::<Vertex>::new(),
                Vec::<Vertex>::new(),
                Vec::<Vertex>::new(),
            ],
            index_vector:  vec![Vec::<u32>::new(), Vec::<u32>::new(), Vec::<u32>::new()],
            noise_map:     OpenSimplex::new()
        }
    }

    pub fn build_procedural_cubes(&mut self)
    {
        use mesh_builder::color::Color;
        use mesh_builder::cube::{Cube, NewNoIndices};
        use nalgebra::{Point3, Rotation3};
        use noise::NoiseFn;

        let dim = 25;
        for z in 0..dim
        {
            for x in 0..dim
            {
                // TODO play around with this
                let scale = 0.05;
                let noise = (self.noise_map
                    .get([f64::from(x) * scale, f64::from(z) * scale])
                    * 10.0)
                    .round();

                let mut cube = Cube::new_no_indices(
                    Point3::new(x as f32, noise as f32, z as f32),
                    Rotation3::identity(),
                    1.0,
                    Color::Clear
                );

                self.vertex_vector[0].append(&mut cube.vertices_sides());
                self.vertex_vector[1].append(&mut cube.vertices_top());
                self.vertex_vector[2].append(&mut cube.vertices_bottom());
            }
        }

        for i in 0..self.vertex_vector.len()
        {
            self.index_vector[i].append(&mut MeshBuilder::gen_indices_for_n_cube_sides(
                (self.vertex_vector[i].len() / 4) as u32
            ));
        }

        assert_eq!(self.vertex_vector.len(), self.index_vector.len());
    }

    fn gen_indices_for_n_cube_sides(amt_sides: u32) -> Vec<u32>
    {
        let index_offsets = [0, 1, 2, 2, 3, 0].iter();
        let mut indices = Vec::<u32>::new();

        for i in 0..amt_sides
        {
            indices.extend(index_offsets.clone().map(|offset| offset + i * 4));
        }

        indices
    }
}
