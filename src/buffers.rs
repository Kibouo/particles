// File: buffers.rs
// Project: src
// Created Date: 14th February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

extern crate std;

use mesh_builder::MeshBuilder;
use mesh_builder::vertex::Vertex;
use std::sync::Arc;
use vulkano::buffer::CpuAccessibleBuffer;
use vulkano::device::Device;
use vulkano::image::attachment::AttachmentImage;

pub struct Buffers
{
    pub vertices: Vec<Arc<CpuAccessibleBuffer<[Vertex]>>>,
    pub indices:  Vec<Arc<CpuAccessibleBuffer<[u32]>>>,
    pub depth:    Arc<AttachmentImage>
}

impl Buffers
{
    pub fn build(
        device: Arc<Device>,
        dimensions: [u32; 2],
        mesh_builder: &MeshBuilder
    ) -> Buffers
    {
        use vulkano::buffer::BufferUsage;
        use vulkano::format::Format;

        let mut vertex_buffers = Vec::<Arc<CpuAccessibleBuffer<[Vertex]>>>::new();
        let mut index_buffers = Vec::<Arc<CpuAccessibleBuffer<[u32]>>>::new();

        for i in 0..mesh_builder.vertex_vector.len()
        {
            vertex_buffers.push(
                CpuAccessibleBuffer::from_iter(
                    device.clone(),
                    BufferUsage::vertex_buffer(),
                    (*mesh_builder
                        .vertex_vector
                        .get(i)
                        .expect("Unable to get vertex vector from mesh builder"))
                        .clone()
                        .into_iter()
                ).expect("Unable to create vertex buffer")
            );

            index_buffers.push(
                CpuAccessibleBuffer::from_iter(
                    device.clone(),
                    BufferUsage::index_buffer(),
                    (*mesh_builder
                        .index_vector
                        .get(i)
                        .expect("Unable to get index vector from mesh builder"))
                        .clone()
                        .into_iter()
                ).expect("Unable to create index buffer")
            );
        }

        let depth_buffer = AttachmentImage::input_attachment(device, dimensions, Format::D16Unorm)
            .expect("Unable to create depth buffer");

        Buffers {
            vertices: vertex_buffers,
            indices:  index_buffers,
            depth:    depth_buffer
        }
    }
}
