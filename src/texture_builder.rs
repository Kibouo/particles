// File: texture_builder.rs
// Project: src
// Created Date: 13th March 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

use std::sync::Arc;
use vulkano::descriptor::PipelineLayoutAbstract;
use vulkano::descriptor::descriptor_set::{PersistentDescriptorSet, PersistentDescriptorSetImg,
                                          PersistentDescriptorSetSampler};
use vulkano::device::{Device, Queue};
use vulkano::format::R8G8B8A8Srgb;
use vulkano::image::ImmutableImage;
use vulkano::sampler::Sampler;
use vulkano::sync::GpuFuture;


type ImageDescriptorSet<PipelineLayout> = PersistentDescriptorSet<
    PipelineLayout,
    (
        (
            (),
            PersistentDescriptorSetImg<Arc<ImmutableImage<R8G8B8A8Srgb>>>
        ),
        PersistentDescriptorSetSampler
    )
>;

pub struct TextureBuilder<PipelineLayout>
{
    pub descriptor_sets: Vec<Arc<ImageDescriptorSet<PipelineLayout>>>,
    sampler:             Arc<Sampler>
}

impl<PipelineLayout> TextureBuilder<PipelineLayout>
where PipelineLayout: PipelineLayoutAbstract
{
    pub fn new(device: Arc<Device>) -> TextureBuilder<PipelineLayout>
    {
        use vulkano::sampler::{Filter, MipmapMode, SamplerAddressMode};

        TextureBuilder {
            descriptor_sets: Vec::<Arc<ImageDescriptorSet<_>>>::new(),
            sampler:         {
                Sampler::new(
                    device,
                    Filter::Linear,
                    Filter::Linear,
                    MipmapMode::Nearest,
                    SamplerAddressMode::Repeat,
                    SamplerAddressMode::Repeat,
                    SamplerAddressMode::Repeat,
                    0.0,
                    1.0,
                    0.0,
                    0.0
                ).expect("Unable to create sampler")
            }
        }
    }

    pub fn build_cube_textures(
        &mut self,
        queue: Arc<Queue>,
        pipeline: PipelineLayout
    ) -> Box<GpuFuture>
    where
        PipelineLayout: Clone
    {
        use texture_loader::TextureLoader;

        let (texture_side, future_0) =
            TextureLoader::open("./textures/side.jpg".to_owned()).image_2d(queue.clone());
        let (texture_grass, future_1) =
            TextureLoader::open("./textures/grass.png".to_owned()).image_2d(queue.clone());
        let (texture_bottom, future_2) =
            TextureLoader::open("./textures/bottom.jpg".to_owned()).image_2d(queue);

        let mut textures = Vec::<Arc<ImmutableImage<R8G8B8A8Srgb>>>::new();
        textures.push(texture_side);
        textures.push(texture_grass);
        textures.push(texture_bottom);

        for texture in textures
        {
            self.descriptor_sets
                .push(Arc::new(texture_to_descriptor_set(
                    pipeline.clone(),
                    texture,
                    self.sampler.clone()
                )));
        }

        Box::new(future_0.join(future_1).join(future_2))
    }
}

fn texture_to_descriptor_set<PipelineLayout>(
    pipeline: PipelineLayout,
    texture: Arc<ImmutableImage<R8G8B8A8Srgb>>,
    sampler: Arc<Sampler>
) -> ImageDescriptorSet<PipelineLayout>
where
    PipelineLayout: PipelineLayoutAbstract
{
    PersistentDescriptorSet::start(pipeline, 0)
        .add_sampled_image(texture, sampler)
        .expect("Unable to add texture to PersistentDescriptorSet")
        .build()
        .expect("Unable to build PersistentDescriptorSet")
}
