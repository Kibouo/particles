#![feature(conservative_impl_trait)]

// File: main.rs
// Project: src
// Created Date: 10th November 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

extern crate image;
extern crate nalgebra;
extern crate noise;
#[macro_use]
extern crate vulkano;
#[macro_use]
extern crate vulkano_shader_derive;
extern crate vulkano_win;
extern crate winit;

mod shaders;
mod vulkan_objects;
mod camera;
mod event_handler;
mod mesh_builder;
mod texture_loader;
mod buffers;
mod frame_counter;
mod texture_builder;

use buffers::Buffers;
use camera::Camera;
use shaders::Shaders;
use texture_builder::TextureBuilder;
use vulkan_objects::VulkanCoreObjects;

fn main()
{
    use mesh_builder::MeshBuilder;
    use nalgebra::Point3;

    let vulkan_objects = VulkanCoreObjects::init();

    let camera = Camera::new(Point3::new(0.0, 5.0, 0.0));

    let mut mesh_builder = MeshBuilder::new();
    mesh_builder.build_procedural_cubes();
    let buffers = Buffers::build(
        vulkan_objects.device.clone(),
        vulkan_objects.dimensions,
        &mesh_builder
    );

    let shaders = Shaders::load(vulkan_objects.device.clone());

    main_loop(vulkan_objects, buffers, camera, &shaders)
}

fn main_loop(
    mut vulkan_objects: VulkanCoreObjects,
    mut buffers: Buffers,
    mut camera: Camera,
    shaders: &Shaders
)
{
    use event_handler::EventHandler;
    use frame_counter::FrameCounter;
    use std::sync::Arc;
    use vulkano::command_buffer::{AutoCommandBufferBuilder, DynamicState};
    use vulkano::format::Format;
    use vulkano::framebuffer::{Framebuffer, Subpass};
    use vulkano::image::attachment::AttachmentImage;
    use vulkano::pipeline::GraphicsPipeline;
    use vulkano::pipeline::viewport::Viewport;
    use vulkano::swapchain;
    use vulkano::swapchain::{AcquireError, SwapchainCreationError};
    use vulkano::sync::{now, FlushError, GpuFuture};

    let render_pass = Arc::new(
        single_pass_renderpass!(
            vulkan_objects.device.clone(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: vulkan_objects.swapchain.format(),
                    samples: 1,
                },
                depth: {
                    load: Clear,
                    store: DontCare,
                    format: Format::D16Unorm,
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {depth}
            }
        ).expect("Unable to create render pass")
    );

    let pipeline = Arc::new(
        GraphicsPipeline::start()
            .vertex_input_single_buffer()
            .vertex_shader(shaders.vertex.main_entry_point(), ())
            .triangle_list()
            .viewports_dynamic_scissors_irrelevant(1)
            .fragment_shader(shaders.fragment.main_entry_point(), ())
            .render_pass(Subpass::from(render_pass.clone(), 0).expect("Unable to create subpass"))
            .cull_mode_back()
            .front_face_counter_clockwise()
            .depth_stencil_simple_depth()
            .build(vulkan_objects.device.clone())
            .expect("Unable to build pipeline")
    );

    let mut texture_builder = TextureBuilder::new(vulkan_objects.device.clone());

    let texture_future =
        texture_builder.build_cube_textures(vulkan_objects.queue.clone(), pipeline.clone());

    let mut previous_frame_end = Box::new(texture_future) as Box<GpuFuture>;

    let mut frame_buffers: Option<Vec<Arc<Framebuffer<_, _>>>> = None;

    camera.calc_vp_matrix(&vulkan_objects.dimensions);

    let mut frame_counter = FrameCounter::start_new();

    let mut event_handler = EventHandler::new();
    event_handler.event_flags.recreate_swapchain = true;

    loop
    {
        previous_frame_end.cleanup_finished();

        if event_handler.event_flags.recreate_swapchain
        {
            vulkan_objects.dimensions = {
                let (width, height) = vulkan_objects
                    .surface
                    .window()
                    .get_inner_size_pixels()
                    .expect("Unable to get window size");
                [width, height]
            };

            buffers.depth = AttachmentImage::input_attachment(
                vulkan_objects.device.clone(),
                vulkan_objects.dimensions,
                Format::D16Unorm
            ).expect("Unable to re-create depth buffer");

            let (new_swapchain, new_images) = match vulkan_objects
                .swapchain
                .recreate_with_dimension(vulkan_objects.dimensions)
            {
                Ok(recreated) => recreated,
                Err(SwapchainCreationError::UnsupportedDimensions) => continue,
                Err(error) => panic!("{:?}", error)
            };

            vulkan_objects.swapchain = new_swapchain;
            vulkan_objects.images = new_images;

            frame_buffers = None;
        }

        if frame_buffers.is_none()
        {
            let new_frame_buffers = Some(
                vulkan_objects
                    .images
                    .iter()
                    .map(|image| {
                        Arc::new(
                            Framebuffer::start(render_pass.clone())
                                .add(image.clone())
                                .expect("Unable to add framebuffer")
                                .add(buffers.depth.clone())
                                .expect("Unable to add depthbuffer")
                                .build()
                                .expect("Unable to build framebuffer")
                        )
                    })
                    .collect::<Vec<_>>()
            );
            frame_buffers = new_frame_buffers;
        }

        let (image_num, acquire_future) =
            match swapchain::acquire_next_image(vulkan_objects.swapchain.clone(), None)
            {
                Ok(recreated) => recreated,
                Err(AcquireError::OutOfDate) =>
                {
                    event_handler.event_flags.recreate_swapchain = true;
                    continue
                },
                Err(error) => panic!("{:?}", error)
            };

        let mut command_buffer_builder = AutoCommandBufferBuilder::primary_one_time_submit(
            vulkan_objects.device.clone(),
            vulkan_objects.queue.family()
        ).expect("Unable to submit to AutoCommandBufferBuilder")
            .begin_render_pass(
                frame_buffers
                    .as_ref()
                    .expect("Unable to pass frame buffers as ref")[image_num]
                    .clone(),
                false,
                vec![[0.0, 0.0, 0.0, 1.0].into(), 1f32.into()]
            )
            .expect("Unable to begin render pass");
        for i in 0..buffers.vertices.len()
        {
            command_buffer_builder = command_buffer_builder
                .draw_indexed(
                    pipeline.clone(),
                    DynamicState {
                        line_width: None,
                        viewports:  Some(vec![
                            Viewport {
                                origin:      [0.0, 0.0],
                                dimensions:  [
                                    vulkan_objects.dimensions[0] as f32,
                                    vulkan_objects.dimensions[1] as f32,
                                ],
                                depth_range: 0.0..1.0
                            },
                        ]),
                        scissors:   None
                    },
                    buffers.vertices[i].clone(),
                    buffers.indices[i].clone(),
                    // descriptor sets
                    texture_builder.descriptor_sets[i].clone(),
                    // push constants
                    camera.homogeneous_vp
                )
                .expect("Failed to draw");
        }
        let command_buffer = command_buffer_builder
            .end_render_pass()
            .expect("Unable to end render pass")
            .build()
            .expect("Unable to end render pass");

        let future = previous_frame_end
            .join(acquire_future)
            .then_execute(vulkan_objects.queue.clone(), command_buffer)
            .expect("Unable to execute command buffer on queue")
            .then_swapchain_present(
                vulkan_objects.queue.clone(),
                vulkan_objects.swapchain.clone(),
                image_num
            )
            .then_signal_fence_and_flush();

        match future
        {
            Ok(fence_signal_future) => previous_frame_end = Box::new(fence_signal_future) as Box<_>,
            // Fixes crash of Swapchain upon resizing of the window
            Err(FlushError::OutOfDate) =>
            {
                event_handler.event_flags.recreate_swapchain = true;
                previous_frame_end = Box::new(now(vulkan_objects.device.clone())) as Box<GpuFuture>
            },
            Err(error) => panic!("{:?}", error)
        }

        event_handler.register_events(&mut vulkan_objects);
        if event_handler.evaluate_events(&mut vulkan_objects, &mut camera)
        {
            return
        }

        camera.calc_vp_matrix(&vulkan_objects.dimensions);

        frame_counter.up();
        if frame_counter.second_passed(1)
        {
            use std::io;
            use std::io::prelude::*;

            print!("FPS: {:?}\r", frame_counter.fps());
            io::stdout().flush().expect("Could not flush stdout");
        }
    }
}
