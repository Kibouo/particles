// File: frame_counter.rs
// Project: src
// Created Date: 14th February 2018
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
//

use std::time::{Duration, Instant};

pub struct FrameCounter
{
    frames: u64,
    time:   Instant
}

impl FrameCounter
{
    pub fn start_new() -> FrameCounter
    {
        FrameCounter {
            frames: 0,
            time:   Instant::now()
        }
    }

    pub fn start(&mut self)
    {
        self.frames = 0;
        self.time = Instant::now();
    }

    pub fn up(&mut self)
    {
        self.frames += 1;
    }

    // fps is an average measurement. Can be called earlier or later than 1sec
    // since last call. Accuracy (too early) or update rate (too late) will be
    // affected.
    pub fn fps(&mut self) -> u64
    {
        let old_frames = self.frames;
        let old_time = self.time;

        self.start();

        old_frames / (self.time - old_time).as_secs()
    }

    pub fn second_passed(
        &self,
        seconds: u64
    ) -> bool
    {
        Instant::now() - self.time >= Duration::from_secs(seconds)
    }
}
