// File: camera.rs
// Project: src
// Created Date: 17th December 2017
// Author: Csonka Mihaly
// E-mail: csonka.mihaly@hotmail.com
// -----
// 

extern crate nalgebra;
extern crate std;

use nalgebra::{Isometry3, Matrix, MatrixArray, Point3, U4, Vector3};

pub struct Camera
{
    pub position:       Point3<f32>,
    pitch:              f32,
    yaw:                f32,
    roll:               f32,
    speed:              f32,
    sensitivity:        f32,
    homogeneous_view:   Matrix<f32, U4, U4, MatrixArray<f32, U4, U4>>,
    pub homogeneous_vp: Matrix<f32, U4, U4, MatrixArray<f32, U4, U4>>
}

impl Camera
{
    pub fn new(inp_position: Point3<f32>) -> Camera
    {
        Camera {
            position:         inp_position,
            pitch:            0.0,
            yaw:              0.0,
            roll:             0.0,
            speed:            0.016,
            sensitivity:      0.032,
            homogeneous_view: nalgebra::zero(),
            homogeneous_vp:   nalgebra::zero()
        }
    }

    // View Projection Matrix
    pub fn calc_vp_matrix(
        &mut self,
        dimensions: &[u32; 2]
    )
    {
        use nalgebra::{Matrix4, Perspective3};

        self.calc_view_matrix();

        let aspect_ratio: f32 = (dimensions[0] as f32) / (dimensions[1] as f32);
        let fov_y = 90.0;
        let projection_perspective = Perspective3::new(aspect_ratio, fov_y, 0.01, 100.0);
        let projection_matrix = projection_perspective.as_matrix();

        // This converts Vulkan 's near & far to be compatible with OpenGL, as well
        // as inverting the y-axis (-y is upwards in Vulkan)
        let correction_matrix = Matrix4::new(
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            -1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.5,
            1.0
        );

        self.homogeneous_vp = correction_matrix * projection_matrix * self.homogeneous_view;
    }

    fn calc_view_matrix(&mut self)
    {
        use nalgebra::{Translation3, UnitQuaternion};

        self.homogeneous_view = Isometry3::from_parts(
            Translation3::from_vector(self.position.coords),
            UnitQuaternion::from_euler_angles(self.pitch, self.yaw, self.roll)
        ).inverse()
            .to_homogeneous();
    }

    pub fn tilt_over_x_axis(
        &mut self,
        angle: f32
    )
    {
        self.pitch += angle * self.sensitivity;
    }

    pub fn tilt_over_y_axis(
        &mut self,
        angle: f32
    )
    {
        self.yaw += angle * self.sensitivity;
    }

    // pub fn tilt_over_z_axis(
    //    &mut self,
    //    angle: f32
    // ) {
    //    self.roll += angle * self.sensitivity;
    // }

    pub fn left(&mut self)
    {
        let direction;
        unsafe {
            direction = Vector3::new(
                *self.homogeneous_view.get_unchecked(0, 0),
                *self.homogeneous_view.get_unchecked(0, 1),
                *self.homogeneous_view.get_unchecked(0, 2)
            );
        }
        self.position -= direction * self.speed;
    }

    pub fn right(&mut self)
    {
        let direction;
        unsafe {
            direction = Vector3::new(
                *self.homogeneous_view.get_unchecked(0, 0),
                *self.homogeneous_view.get_unchecked(0, 1),
                *self.homogeneous_view.get_unchecked(0, 2)
            );
        }
        self.position += direction * self.speed;
    }

    pub fn down(&mut self)
    {
        let direction;
        unsafe {
            direction = Vector3::new(
                *self.homogeneous_view.get_unchecked(1, 0),
                *self.homogeneous_view.get_unchecked(1, 1),
                *self.homogeneous_view.get_unchecked(1, 2)
            );
        }
        self.position -= direction * self.speed;
    }

    pub fn up(&mut self)
    {
        let direction;
        unsafe {
            direction = Vector3::new(
                *self.homogeneous_view.get_unchecked(1, 0),
                *self.homogeneous_view.get_unchecked(1, 1),
                *self.homogeneous_view.get_unchecked(1, 2)
            );
        }
        self.position += direction * self.speed;
    }

    pub fn forward(&mut self)
    {
        let direction;

        unsafe {
            direction = Vector3::new(
                *self.homogeneous_view.get_unchecked(2, 0),
                *self.homogeneous_view.get_unchecked(2, 1),
                *self.homogeneous_view.get_unchecked(2, 2)
            );
        }
        self.position -= direction * self.speed;
    }

    pub fn backward(&mut self)
    {
        let direction;
        unsafe {
            direction = Vector3::new(
                *self.homogeneous_view.get_unchecked(2, 0),
                *self.homogeneous_view.get_unchecked(2, 1),
                *self.homogeneous_view.get_unchecked(2, 2)
            );
        }
        self.position += direction * self.speed;
    }
}
